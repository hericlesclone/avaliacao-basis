package br.com.basis.prova.recurso;

import br.com.basis.prova.dominio.Aluno;
import br.com.basis.prova.servico.AlunoServico;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/alunos")
public class AlunoRecurso {
    private final AlunoServico alunoServico;
    
    //Construtor
    public AlunoRecurso(AlunoServico alunoServico) {
        this.alunoServico = alunoServico;
    }

    //Criar registro de um novo aluno
    @PostMapping
    public ResponseEntity<?> salvar(@RequestBody Aluno aluno) throws URISyntaxException {
        return ResponseEntity.ok(alunoServico.salvar(aluno));
    }

    //Alterar os dados de um aluno
    @PutMapping
    public ResponseEntity<?> editar(@RequestBody Aluno aluno) throws URISyntaxException {
        return ResponseEntity.ok(alunoServico.editar(aluno));
    }

    //Exclusão de um aluno por sua matricula
    @DeleteMapping("/{matricula}")
    public ResponseEntity<?> excluir(@PathVariable("matricula") String matricula) {
        return ResponseEntity.ok(alunoServico.excluir(matricula));
    }
    
    // Consulta Aluno
    @GetMapping
    public ResponseEntity<?> consultar() {
        return ResponseEntity.ok(alunoServico.consultar());
    }
    
    // Consulta AlunoListagemDTO
    @GetMapping("/detalhes")
    public ResponseEntity<?> consultar2() {
        return ResponseEntity.ok(alunoServico.consultarListagem());
    }
    
    //Consulta AlunoDetalhadoDTO
    @GetMapping("/detalhes/{id}")
    public ResponseEntity<?> detalhar(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(alunoServico.detalhar(id));
    }

}
