package br.com.basis.prova.recurso;

import br.com.basis.prova.dominio.Professor;
import br.com.basis.prova.servico.ProfessorServico;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/professores")
public class ProfessorRecurso {
    private final ProfessorServico professorServico;
    
    //Contrutor
    public ProfessorRecurso(ProfessorServico professorServico) {
        this.professorServico = professorServico;
    }

    //Criar registro de um novo professor
    @PostMapping
    public ResponseEntity<?> salvar(@RequestBody Professor professor) throws URISyntaxException {
    	return ResponseEntity.ok(professorServico.salvar(professor));
    }

    //Alterar os dados de um professor
    @PutMapping
    public ResponseEntity<?> editar(@RequestBody Professor professor) throws URISyntaxException {
        return ResponseEntity.ok(professorServico.editar(professor));
    }

    //Exclusão de um professor por sua matricula
    @DeleteMapping("/{matricula}")
    public ResponseEntity<?> excluir(@PathVariable("matricula") String matricula) {
    	// O professor só pode ser excluido se não for responsável por nenhuma disciplina
    	return ResponseEntity.ok(professorServico.excluir(matricula));
    }

    //Consulta Professor
    @GetMapping
    public ResponseEntity<?> consultar() {
        return ResponseEntity.ok(professorServico.consultar());
    }

    //Consulta ProfessorDetalhadoDTO
    @GetMapping("/detalhes/{id}")
    public ResponseEntity<?> detalhar(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(professorServico.detalhar(id));
    }

}
