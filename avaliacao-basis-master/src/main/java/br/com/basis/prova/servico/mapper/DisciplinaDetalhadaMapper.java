package br.com.basis.prova.servico.mapper;

import br.com.basis.prova.dominio.Disciplina;
import br.com.basis.prova.dominio.Professor;
import br.com.basis.prova.dominio.dto.DisciplinaDetalhadaDTO;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {DisciplinaListagemMapper.class})
public class DisciplinaDetalhadaMapper implements EntityMapper<DisciplinaDetalhadaDTO, Disciplina> {

	@Override
	public Disciplina toEntity(DisciplinaDetalhadaDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DisciplinaDetalhadaDTO toDto(Disciplina entity) {
		DisciplinaDetalhadaDTO disciplina_detalhada = new DisciplinaDetalhadaDTO();
		disciplina_detalhada.setId(entity.getId());
		disciplina_detalhada.setNome(entity.getNome());
		disciplina_detalhada.setCargaHoraria(entity.getCargaHoraria());
		disciplina_detalhada.setProfessor(new Professor());
		return disciplina_detalhada;
	}

	@Override
	public List<Disciplina> toEntity(List<DisciplinaDetalhadaDTO> dtoList) {
		List<Disciplina> disciplinas = new ArrayList<>();
		for (DisciplinaDetalhadaDTO prof_dto : dtoList) { 
			disciplinas.add(toEntity(prof_dto));
		}
		return disciplinas;
	}

	@Override
	public List<DisciplinaDetalhadaDTO> toDto(List<Disciplina> entityList) {
		List<DisciplinaDetalhadaDTO> disciplinas_detalhada = new ArrayList<>();
		for (Disciplina disc : entityList) { 
			disciplinas_detalhada.add(toDto(disc));
		}
		return disciplinas_detalhada;
	}
}
