package br.com.basis.prova.recurso;

import br.com.basis.prova.dominio.Disciplina;
import br.com.basis.prova.servico.DisciplinaServico;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api/disciplinas")
public class DisciplinaRecurso {
    private final DisciplinaServico disciplinaServico;

    public DisciplinaRecurso(DisciplinaServico disciplinaServico) {
        this.disciplinaServico = disciplinaServico;
    }

    //Criar registro de uma nova disciplina
    @PostMapping
    public ResponseEntity<?> salvar(@RequestBody Disciplina disciplina) throws URISyntaxException {
    	return ResponseEntity.ok(disciplinaServico.salvar(disciplina));
    }

    //Alterar os dados de uma disciplina
    @PutMapping
    public ResponseEntity<?> editar(@RequestBody Disciplina disciplina) throws URISyntaxException {
    	return ResponseEntity.ok(disciplinaServico.editar(disciplina));
    }

    //Exclusão de uma disciplina pelo identificador
    @DeleteMapping("/{id}")
    public ResponseEntity<?> excluir(@PathVariable("id") Integer id) {
    	// A disciplina só pode ser excluída de não tivernenhum aluno matriculado nela
    	return ResponseEntity.ok(disciplinaServico.excluir(id));
    }

    // Consulta DisciplinaListagemDTO
    @GetMapping
    public ResponseEntity<?> consultar() {
        return ResponseEntity.ok(disciplinaServico.consultar());
    }
    
 // Consulta DisciplinaListagemDTO
    @GetMapping("/detalhes")
    public ResponseEntity<?> consultar2() {
        return ResponseEntity.ok(disciplinaServico.consultar2());
    }

    // Consulta DisciplinaDetalhadaDTO
    @GetMapping("/detalhes/{id}")
    public ResponseEntity<?> detalhar(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(disciplinaServico.detalhar(id));
    }

}
