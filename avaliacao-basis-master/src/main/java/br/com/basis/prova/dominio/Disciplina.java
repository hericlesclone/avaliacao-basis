package br.com.basis.prova.dominio;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "DISCIPLINA")
@Getter
@Setter
@NoArgsConstructor
public class Disciplina{ //Informações da Disciplina
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Integer id;

	@Column(name = "NOME", nullable = false, length = 50)
    private String nome;

	@Column(name = "DESCRICAO", nullable = false, length = 200)
    private String descricao;

	@Column(name = "CARGA_HORARIA", nullable = false)
    private Integer cargaHoraria;

	@Column(name = "ATIVA", nullable = false)
    private Integer ativa;

	@ManyToOne
	@JoinColumn(name = "ID_PROFESSOR", referencedColumnName = "ID")
    private Professor professor;

}
