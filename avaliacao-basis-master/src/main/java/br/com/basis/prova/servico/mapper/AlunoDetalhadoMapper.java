package br.com.basis.prova.servico.mapper;

import br.com.basis.prova.dominio.Aluno;
import br.com.basis.prova.dominio.dto.AlunoDetalhadoDTO;
import java.util.ArrayList;
import java.util.List;

public class AlunoDetalhadoMapper implements EntityMapper<AlunoDetalhadoDTO, Aluno> {

	@Override
	public Aluno toEntity(AlunoDetalhadoDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AlunoDetalhadoDTO toDto(Aluno entity) {
		AlunoDetalhadoDTO alunoDetalhadoDTO = new AlunoDetalhadoDTO();
		alunoDetalhadoDTO.setId(entity.getId());
		alunoDetalhadoDTO.setNome(entity.getNome());
		alunoDetalhadoDTO.setMatricula(entity.getMatricula());
		alunoDetalhadoDTO.setDisciplinas(entity.getDisciplinas());
		return alunoDetalhadoDTO;
	}

	@Override
	public List<Aluno> toEntity(List<AlunoDetalhadoDTO> dtoList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AlunoDetalhadoDTO> toDto(List<Aluno> entityList) {
		List<AlunoDetalhadoDTO> alunos_detalhados = new ArrayList<>();
		for (Aluno disc : entityList) { 
			alunos_detalhados.add(toDto(disc));
		}
		return alunos_detalhados;
	}
}
