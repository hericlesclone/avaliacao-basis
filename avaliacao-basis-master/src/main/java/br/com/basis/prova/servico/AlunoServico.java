package br.com.basis.prova.servico;

import br.com.basis.prova.dominio.Aluno;
import br.com.basis.prova.dominio.dto.AlunoDetalhadoDTO;
import br.com.basis.prova.dominio.dto.AlunoListagemDTO;
import br.com.basis.prova.repositorio.AlunoRepositorio;
import br.com.basis.prova.servico.mapper.AlunoDetalhadoMapper;
import br.com.basis.prova.servico.mapper.AlunoListagemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AlunoServico {
    private AlunoListagemMapper alunoListagemMapper;
    private AlunoDetalhadoMapper alunoDetalhadoMapper;
    
	@Autowired
    private AlunoRepositorio alunoRepositorio;

	//Construtor
    public AlunoServico(AlunoRepositorio alunoRepositorio) {
    	this.alunoDetalhadoMapper = new AlunoDetalhadoMapper();
        this.alunoListagemMapper = new AlunoListagemMapper();
        this.alunoRepositorio = alunoRepositorio;
    }

    public String salvar(Aluno aluno) {
    	Optional<Aluno> get_aluno = alunoRepositorio.findByMatricula(aluno.getMatricula());
    	
    	if(get_aluno.isPresent()){//aluno com essa matricula já existir
        	return "ERRO: Já existe um aluno com esse mesmo número de matricula: " + aluno.getMatricula() + " !";
        }
    	
		if(verificarCPF(aluno)){
        	return "ERRO: Já existe um aluno cadastrado com o cpf " + aluno.getCpf() + " no sistema!";
        }
		
		Aluno new_aluno = new Aluno();
		new_aluno = aluno;
    	alunoRepositorio.save(new_aluno);
    	return "O Aluno " + new_aluno.getNome() + " foi cadastrado com sucesso!";
    }
    
    public String editar(Aluno aluno) {
    	Optional<Aluno> get_aluno = alunoRepositorio.findByMatricula(aluno.getMatricula());
    	
    	if(!get_aluno.isPresent()){//aluno com essa matricula não existe
        	return "ERRO: O Aluno com a matricula " + aluno.getMatricula() + " não está cadastrado!";
        }
    	
    	//se o cpf já existe e não é o próprio cpf do aluno
		if(verificarCPF(aluno) && !(aluno.getCpf().equals(get_aluno.get().getCpf()))){
        	return "ERRO: Já existem um aluno cadastrado com o cpf " + aluno.getCpf() + " no sistema!";
        }
		
		alunoRepositorio.findById(get_aluno.get().getId()).map(record->{
			record.setMatricula(aluno.getMatricula());
			record.setCpf(aluno.getCpf());
			record.setNome(aluno.getNome());
			record.setDataNascimento(aluno.getDataNascimento());
			record.setDisciplinas(aluno.getDisciplinas());
			return "Os dados do aluno " + get_aluno.get().getNome() + " foram alterados com sucesso!";
		});
		
		return "Os dados do aluno " + get_aluno.get().getNome() + " foram alterados com sucesso!";
    }

    private boolean verificarCPF(Aluno aluno) {
        Aluno alunoCpf = alunoRepositorio.findByCpf(aluno.getCpf());
        if((alunoCpf != null) && (alunoCpf.getCpf().equals(aluno.getCpf()))){
        	return true;
        }
        return false;
    }

    public String excluir(String matricula) {
    	String aluno_excluido = null;
        Optional<Aluno> aluno = alunoRepositorio.findByMatricula(matricula);
        
        if(!aluno.isPresent()){
        	return "ERRO: O Aluno com a matricula " + matricula + " não está cadastrado!";
        }
        
        if(aluno.get().getDisciplinas().isEmpty()){
        	aluno_excluido = aluno.get().getNome();
        	alunoRepositorio.deleteById(aluno.get().getId());
        	return "O Aluno " + aluno_excluido + " foi excluído com sucesso!";
        }
        
        return "ERRO: O Aluno " + aluno.get().getNome() + " ainda está vinculado a uma disciplina!";
    }

    public List<Aluno> consultar() {
    	return alunoRepositorio.findAll();
    }
    
    public List<AlunoListagemDTO> consultarListagem() {
    	return alunoListagemMapper.toDto(alunoRepositorio.findAll());
    }

    public AlunoDetalhadoDTO detalhar(Integer id) {
        Optional<Aluno> aluno = alunoRepositorio.findById(id);
        AlunoDetalhadoDTO aluno_detalhado = alunoDetalhadoMapper.toDto(aluno.get());
        return aluno_detalhado;
    }
}
