package br.com.basis.prova.servico.mapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import br.com.basis.prova.dominio.Aluno;
import br.com.basis.prova.dominio.dto.AlunoListagemDTO;

public class AlunoListagemMapper implements EntityMapper<AlunoListagemDTO, Aluno>{

	@Override
	public Aluno toEntity(AlunoListagemDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AlunoListagemDTO toDto(Aluno entity) {
		AlunoListagemDTO alunoListagemDTO = new AlunoListagemDTO();
		alunoListagemDTO.setId(entity.getId());
		alunoListagemDTO.setNome(entity.getNome());
		alunoListagemDTO.setMatricula(entity.getMatricula());
		
		//idade
		SimpleDateFormat y = new SimpleDateFormat("yyyy");
        SimpleDateFormat m = new SimpleDateFormat("MM");
        SimpleDateFormat d = new SimpleDateFormat("dd");
        
        int ano1 = entity.getDataNascimento().getYear();
        int ano2 = Integer.parseInt(y.format(new Date()));
        int idade = ano2 - ano1;
        
        int mes1 = entity.getDataNascimento().getMonth().getValue();
        int mes2 = Integer.parseInt(m.format(new Date()));
        
        int dia1 = entity.getDataNascimento().getDayOfMonth();
        int dia2 = Integer.parseInt(d.format(new Date()));
        
        if (mes2 < mes1){
        	idade --;
        } else {
          if(dia2 < dia1){
        	  idade --;
        	}
        }
		
        alunoListagemDTO.setIdade(idade);
		
		return alunoListagemDTO;
	}

	@Override
	public List<Aluno> toEntity(List<AlunoListagemDTO> dtoList) {
		List<Aluno> alunos = new ArrayList<>();
		for (AlunoListagemDTO prof_dto : dtoList) { 
			alunos.add(toEntity(prof_dto));
		}
		return alunos;
	}

	@Override
	public List<AlunoListagemDTO> toDto(List<Aluno> entityList) {
		List<AlunoListagemDTO> alunos_dto = new ArrayList<>();
		for (Aluno disc : entityList) { 
			alunos_dto.add(toDto(disc));
		}
		return alunos_dto;
	}

}
