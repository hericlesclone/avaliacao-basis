package br.com.basis.prova.servico;

import br.com.basis.prova.dominio.Aluno;
import br.com.basis.prova.dominio.Disciplina;
import br.com.basis.prova.dominio.Professor;
import br.com.basis.prova.dominio.dto.DisciplinaDetalhadaDTO;
import br.com.basis.prova.dominio.dto.DisciplinaListagemDTO;
import br.com.basis.prova.repositorio.AlunoRepositorio;
import br.com.basis.prova.repositorio.DisciplinaRepositorio;
import br.com.basis.prova.repositorio.ProfessorRepositorio;
import br.com.basis.prova.servico.mapper.DisciplinaDetalhadaMapper;
import br.com.basis.prova.servico.mapper.DisciplinaListagemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DisciplinaServico {
	@Autowired
    private DisciplinaRepositorio disciplinaRepositorio;
    private DisciplinaListagemMapper disciplinaListagemMapper;
    private DisciplinaDetalhadaMapper disciplinaDetalhadaMapper;

    @Autowired
    private ProfessorRepositorio professorRepositorio;
    
    @Autowired
    private AlunoRepositorio alunoRepositorio;

    public DisciplinaServico(DisciplinaRepositorio disciplinaRepositorio) {
        this.disciplinaListagemMapper = new DisciplinaListagemMapper();
        this.disciplinaDetalhadaMapper = new DisciplinaDetalhadaMapper();
        this.disciplinaRepositorio = disciplinaRepositorio;
    }

    //Função que cadastra um novo professor
    public String salvar(Disciplina disciplina) {
    	//Já vou ferificar no front se existe um professor linkado a disciplina nova que será cadastrada
		Optional<Disciplina> get_disciplina = disciplinaRepositorio.findByNome(disciplina.getNome());
		
		if(get_disciplina.isPresent()){//disciplina com esse nome já existir
        	return "ERRO: Já existe uma disciplina com esse nome: " + disciplina.getNome() + " !";
        }
		
		Disciplina new_disciplina = new Disciplina();
		new_disciplina = disciplina;
		disciplinaRepositorio.save(new_disciplina);
    	return "A disciplina " + new_disciplina.getNome() + " foi cadastrada com sucesso!";
    }
    
    //Função que edita os dados de um professor
    public String editar(Disciplina disciplina) {
    	Optional<Disciplina> get_disciplina = disciplinaRepositorio.findByNome(disciplina.getNome());

		if(!get_disciplina.isPresent()){//disciplina com esse nome já existir
        	return "ERRO: A disciplina com o nome " + disciplina.getNome() + " não está cadastrada!";
        }
		
    	disciplinaRepositorio.findById(get_disciplina.get().getId()).map(record->{
    		record.setNome(disciplina.getNome());
    		record.setDescricao(disciplina.getDescricao());
    		record.setCargaHoraria(disciplina.getCargaHoraria());
    		record.setAtiva(disciplina.getAtiva());
    		record.setProfessor(disciplina.getProfessor());
    		return "Os dados da disciplina " + get_disciplina.get().getNome() + " foram alterados com sucesso!";
    	});
    	return "Os dados da disciplina " + get_disciplina.get().getNome() + " foram alterados com sucesso!";
    }

    //Função que exclui uma disciplina
    public String excluir(Integer id) {
    	String disciplina_excluida = null;
    	Optional<Disciplina> disciplina = disciplinaRepositorio.findById(id);
        
        if(!disciplina.isPresent()){
        	return "ERRO: A disciplina com o identificador " + id + " não está cadastrada!";
        }
        
        List<Aluno> alunos = alunoRepositorio.findAllByDisciplinas(disciplina.get());
        
        if(alunos.isEmpty()){
        	disciplina_excluida = disciplina.get().getNome();
        	disciplinaRepositorio.deleteById(disciplina.get().getId());
        	return "A disciplina " + disciplina_excluida + " foi excluída com sucesso!";
        }
        
    	return "ERRO: A disciplina " + disciplina.get().getNome() + " ainda possui alunos matriculados!";
    }

  //Função que retorna todas as disciplinas como DisciplinaListagemDTO
    public List<Disciplina> consultar() {
        return disciplinaRepositorio.findAll();
    }
    
    //Função que retorna todas as disciplinas como DisciplinaListagemDTO
    public List<DisciplinaListagemDTO> consultar2() {
        return disciplinaListagemMapper.toDto(disciplinaRepositorio.findAll());
    }

    //Função que retorna uma disciplina como DisciplinaDetalhadaDTO
    public DisciplinaDetalhadaDTO detalhar(Integer id) {
    	Optional<Disciplina> disciplina = disciplinaRepositorio.findById(id);
    	DisciplinaDetalhadaDTO disciplina_detalhada = disciplinaDetalhadaMapper.toDto(disciplina.get());
    	Optional<Professor> professor = professorRepositorio.findById(disciplina.get().getProfessor().getId());
    	disciplina_detalhada.setProfessor(professor.get()); 
    	return disciplina_detalhada;
    }

}
