package br.com.basis.prova.servico;

import br.com.basis.prova.dominio.Disciplina;
import br.com.basis.prova.dominio.Professor;
import br.com.basis.prova.dominio.dto.ProfessorDTO;
import br.com.basis.prova.dominio.dto.ProfessorDetalhadoDTO;
import br.com.basis.prova.repositorio.DisciplinaRepositorio;
import br.com.basis.prova.repositorio.ProfessorRepositorio;
import br.com.basis.prova.servico.mapper.ProfessorDetalhadoMapper;
import br.com.basis.prova.servico.mapper.ProfessorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProfessorServico {

	@Autowired
    private ProfessorRepositorio professorRepositorio;
    private ProfessorMapper professorMapper;
    private ProfessorDetalhadoMapper professorDetalhadoMapper;
    
    @Autowired
    private DisciplinaRepositorio disciplinaRepositorio;

    //Construtor
    public ProfessorServico(ProfessorRepositorio professorRepositorio) {
		this.professorMapper = new ProfessorMapper();
		this.professorDetalhadoMapper = new ProfessorDetalhadoMapper();
        this.professorRepositorio = professorRepositorio;
    }

    //Função que cadastra um novo professor
    public String salvar(Professor professor) {
    	Optional<Professor> prof = professorRepositorio.findByMatricula(professor.getMatricula());
    	
    	if(prof.isPresent()){//professor com essa matricula existir
        	return "ERRO: Já existe um professor com esse mesmo número de matricula: " + professor.getMatricula() + " !";
        }
    	
		Professor new_professor = new Professor();
    	new_professor = professor;
    	professorRepositorio.save(new_professor);
    	return "O professor " + new_professor.getNome() + " foi cadastrado com sucesso!";
    }
    
  //Função que edita os dados de um professor
    public String editar(Professor professor) {
    	Optional<Professor> prof = professorRepositorio.findByMatricula(professor.getMatricula());
    	
    	if(!prof.isPresent()){//professor com essa matricula não existe
        	return "ERRO: O Professor com a matricula " + professor.getMatricula() + " não está cadastrado!";
        }
    	
    	//Atualizar um cadastro já existente
		professorRepositorio.findById(prof.get().getId()).map(record->{
			record.setArea_atuacao(professor.getArea_atuacao());
			record.setDataNascimento(professor.getDataNascimento());
			record.setMatricula(professor.getMatricula());
			record.setNome(professor.getNome());
			return "Os dados do professor " + prof.get().getNome() + " foram alterados com sucesso!";
		});
		return "Os dados do professor " + prof.get().getNome() + " foram alterados com sucesso!";
    }

    //Função que exclui um professor
    public String excluir(String matricula) {
    	String professor_excluido = null;
        Optional<Professor> professor = professorRepositorio.findByMatricula(matricula);
        
        if(!professor.isPresent()){
        	return "ERRO: O Professor com a matricula " + matricula + " não está cadastrado!";
        }
        
        List<Disciplina> disciplinas = disciplinaRepositorio.findByProfessor(professor.get());
        
        if(disciplinas.isEmpty()){
        	professor_excluido = professor.get().getNome();
        	professorRepositorio.deleteById(professor.get().getId());
        	return "O Professor " + professor_excluido + " foi excluído com sucesso!";
        }
        else {
        	return "ERRO: O Professor " + professor.get().getNome() + " ainda está associado a uma disciplina!";
        }
    }

    //Função que retorna todos os professores como ProfessorDTO
	public List<Professor> consultar() {
		return professorRepositorio.findAll();
    }

	//Função que retorna um professor como ProfessorDetalhadoDTO
    public ProfessorDetalhadoDTO detalhar(Integer id) {
    	Optional<Professor> professor = professorRepositorio.findById(id);
    	ProfessorDetalhadoDTO professorDetalhadoDTO = professorDetalhadoMapper.toDto(professor.get());
    	List<Disciplina> disciplinas = disciplinaRepositorio.findByAtivas(id);
    	professorDetalhadoDTO.setDisciplinas(disciplinas);
        return professorDetalhadoDTO;
    }

}
