package br.com.basis.prova.dominio.dto;

import java.util.ArrayList;
import java.util.List;
import br.com.basis.prova.dominio.Disciplina;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ProfessorDetalhadoDTO { // DTO usado para a consulta detalhada de um professor
	
    private Integer id;
    private String nome;
    private String matricula;
    private List<Disciplina> disciplinas = new ArrayList<>();
}
