package br.com.basis.prova.servico.mapper;

import br.com.basis.prova.dominio.Professor;
import br.com.basis.prova.dominio.dto.ProfessorDetalhadoDTO;
import br.com.basis.prova.repositorio.DisciplinaRepositorio;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {DisciplinaListagemMapper.class})
public class ProfessorDetalhadoMapper implements EntityMapper<ProfessorDetalhadoDTO, Professor> {
	
	DisciplinaRepositorio disciplinarepositorio;

	@Override
	public Professor toEntity(ProfessorDetalhadoDTO dto) {
		Professor professor = new Professor();
		professor.setId(dto.getId());
		professor.setNome(dto.getNome());
		professor.setMatricula(dto.getMatricula());
		return professor;
	}

	@Override
	public ProfessorDetalhadoDTO toDto(Professor entity) {
		ProfessorDetalhadoDTO professorDetalhadoDTO = new ProfessorDetalhadoDTO();
		professorDetalhadoDTO.setId(entity.getId());
		professorDetalhadoDTO.setNome(entity.getNome());
		professorDetalhadoDTO.setMatricula(entity.getMatricula());
		professorDetalhadoDTO.setDisciplinas(new ArrayList<>());
		return professorDetalhadoDTO;
	}

	@Override
	public List<Professor> toEntity(List<ProfessorDetalhadoDTO> dtoList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProfessorDetalhadoDTO> toDto(List<Professor> entityList) {
		// TODO Auto-generated method stub
		return null;
	}
}
