package br.com.basis.prova.servico.mapper;

import br.com.basis.prova.dominio.Professor;
import br.com.basis.prova.dominio.dto.ProfessorDTO;
import java.util.ArrayList;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {DisciplinaMapper.class})
public class ProfessorMapper implements EntityMapper<ProfessorDTO, Professor> {

	@Override
	public Professor toEntity(ProfessorDTO dto) {
		Professor professor = new Professor();
		professor.setArea_atuacao(dto.getArea());
		professor.setDataNascimento(dto.getDataNascimento());
		professor.setId(dto.getId());
		professor.setMatricula(dto.getMatricula());
		professor.setNome(dto.getNome());
		return professor;
	}

	@Override
	public ProfessorDTO toDto(Professor entity) {
		ProfessorDTO professorDTO = new ProfessorDTO();
		professorDTO.setArea(entity.getArea_atuacao());
		professorDTO.setDataNascimento(entity.getDataNascimento());
		professorDTO.setId(entity.getId());
		professorDTO.setMatricula(entity.getMatricula());
		professorDTO.setNome(entity.getNome());
		return professorDTO;
	}

	@Override
	public List<Professor> toEntity(List<ProfessorDTO> dtoList) {
		List<Professor> professores = new ArrayList<>();
		for (ProfessorDTO prof_dto : dtoList) { 
			professores.add(toEntity(prof_dto));
		}
		return professores;
	}

	@Override
	public List<ProfessorDTO> toDto(List<Professor> entityList) {
		List<ProfessorDTO> professores_dto = new ArrayList<>();
		for (Professor prof : entityList) { 
			professores_dto.add(toDto(prof));
		}
		return professores_dto;
	}
}
