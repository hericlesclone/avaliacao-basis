package br.com.basis.prova.dominio.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class DisciplinaListagemDTO { // DTO usado para consulta simples de disciplinas

    private Integer id;
    private String nome;
    private String descricao;
    private Integer cargaHoraria;

}
