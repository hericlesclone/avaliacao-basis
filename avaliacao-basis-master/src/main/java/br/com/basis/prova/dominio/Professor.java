package br.com.basis.prova.dominio;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "PROFESSOR")
@Getter
@Setter
@NoArgsConstructor
public class Professor{ //Informações do Professor
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Integer id;

	@Column(name = "NOME", nullable = false, length = 50)
    private String nome;

	@Column(name = "MATRICULA", nullable = false, length = 6)
    private String matricula;

	@Column(name = "AREA_ATUACAO", nullable = false, length = 200)
    private String area_atuacao;

	@Column(name = "DATA_NASCIMENTO", nullable = false)
    private LocalDate dataNascimento;
}
