package br.com.basis.prova.servico.mapper;

import br.com.basis.prova.dominio.Disciplina;
import br.com.basis.prova.dominio.dto.DisciplinaListagemDTO;
import java.util.ArrayList;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public class DisciplinaListagemMapper implements EntityMapper<DisciplinaListagemDTO, Disciplina> {

	@Override
	public Disciplina toEntity(DisciplinaListagemDTO dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DisciplinaListagemDTO toDto(Disciplina entity) {
		DisciplinaListagemDTO disciplinaDTO = new DisciplinaListagemDTO();
		disciplinaDTO.setId(entity.getId());
		disciplinaDTO.setNome(entity.getNome());
		disciplinaDTO.setDescricao(entity.getDescricao());
		disciplinaDTO.setCargaHoraria(entity.getCargaHoraria());
		return disciplinaDTO;
	}

	@Override
	public List<Disciplina> toEntity(List<DisciplinaListagemDTO> dtoList) {
		List<Disciplina> disciplinas = new ArrayList<>();
		for (DisciplinaListagemDTO prof_dto : dtoList) { 
			disciplinas.add(toEntity(prof_dto));
		}
		return disciplinas;
	}

	@Override
	public List<DisciplinaListagemDTO> toDto(List<Disciplina> entityList) {
		List<DisciplinaListagemDTO> disciplinas_dto = new ArrayList<>();
		for (Disciplina disc : entityList) { 
			disciplinas_dto.add(toDto(disc));
		}
		return disciplinas_dto;
	}
}
