import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlunoService } from '../services/aluno.service';
import { ConfirmationService } from 'primeng/api';
import { Aluno } from '../models/aluno.model';
import { AlunoListagemDTO } from '../models/aluno.ListagemDTO';
import { AlunoDetalhadoDTO } from '../models/aluno.DetalhadoDTO';

@Component({
  selector: 'app-aluno-list',
  templateUrl: './aluno-list.component.html',
  styleUrls: ['./aluno-list.component.css']
})
export class AlunoListComponent implements OnInit {

  alunos: Aluno[];
  alunoDetalhado: AlunoDetalhadoDTO;
  alunoListagemDTO: AlunoListagemDTO;
  texto: string;

  constructor(
    private alunoService: AlunoService,
    private confirmationService: ConfirmationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.listarAlunos();
  }

  listarAlunos() {
    this.alunoService.listar().subscribe(dados => {
      this.alunos = dados;
    });
  }

  deletar(aluno: Aluno) {
    this.alunoService.deletar(aluno.matricula).subscribe(dados => {
      alert(dados);
      this.listarAlunos();
    }, erro => {
      alert(erro.error.text);
      this.listarAlunos();
    });
  }

  getListaAlunos(): Aluno[]{
    return this.alunos;
  }

  ListagemDTO(id: number){
    this.alunoService.getListagemDTO(id).subscribe(dados => {
      this.alunoListagemDTO = dados;
      alert('Id: ' + this.alunoListagemDTO.id + '\n'
            + 'Nome: ' + this.alunoListagemDTO.nome + '\n'
            + 'Matrícula: ' + this.alunoListagemDTO.matricula + '\n'
            + 'Idade: ' + this.alunoListagemDTO.idade + '\n');
    });
  }

  DetalhadoDTO(id: number){
    this.alunoService.getDetalhadoDTO(id).subscribe(dados => {
      this.alunoDetalhado = dados;
      this.texto = 'Id: ' + this.alunoDetalhado.id + '\n'
                + 'Nome: ' + this.alunoDetalhado.nome + '\n'
                + 'Matrícula: ' + this.alunoDetalhado.matricula + '\n';
      this.alunoDetalhado.disciplinas.map(disciplina =>{
        this.texto = this.texto + '   - Disciplina: ' + disciplina.nome + '\n'
                                + '      - id: ' + disciplina.id + '\n'
                                + '      - descrição: ' + disciplina.descricao + '\n'
                                + '      - ativa: ' + disciplina.ativa + '\n'
                                + '      - carga horária: ' + disciplina.cargaHoraria + 'hrs\n'
                                + '      - professor: ' + disciplina.professor.nome + '\n';
      });
      alert(this.texto);
    });
  }

}
