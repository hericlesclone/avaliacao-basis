import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Aluno } from '../models/aluno.model';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AlunoService {
  private url = 'http://localhost:8080/api/alunos/';
  alunos: Aluno[] = [];
  aluno: Aluno;

  constructor(
    private http: HttpClient, 
    private datePipe: DatePipe) { }

  //obtem lista de alunos
  listar(): Observable<any>{
    return this.http.get(this.url);
  }

  // apaga aluno por matricula
  deletar(matricula): Observable<any>{
    return this.http.delete(this.url + matricula);
  }

  save(aluno: Aluno, title : string) {
    if(title === "Cadastro"){
      this.store(aluno).subscribe(dados => {//cadastrar
        alert(dados);
      }, erro => {
        alert(erro.error.text);
      });
    }
    else{
      this.update(aluno).subscribe(dados => {//atualizar
        alert(dados);
      }, erro => {
        alert(erro.error.text);
      });
    }
  }

  getDisciplinas() {
    // obtem lista de disciplinas do aluno
    return [];
  }

  obterAluno(id : number): Aluno{
    this.listar().subscribe(dados => {
      this.alunos = dados;
      this.alunos.map(aluno => {
        if(aluno.id == id){
          this.aluno = aluno;
          this.aluno.dataNascimento = new Date(this.datePipe.transform(aluno.dataNascimento, 'dd-MM-yyyy'));
        }
      });
    });
    return this.aluno;
  }

  //salva um novo aluno
  store(aluno: Aluno): Observable<any>{
    aluno.dataNascimento = new Date(this.datePipe.transform(aluno.dataNascimento, 'yyyy-MM-dd'));
    return this.http.post(this.url, aluno);
  }

  // atualiza aluno
  update(aluno: Aluno): Observable<any> {
    aluno.dataNascimento = new Date(this.datePipe.transform(aluno.dataNascimento, 'yyyy-MM-dd'));
    return this.http.put(this.url, aluno);
  }

  getListagemDTO(id: number): Observable<any>{
    return this.http.get(this.url + 'listagem/' + id);
  }

  getDetalhadoDTO(id: number): Observable<any>{
    return this.http.get(this.url + 'detalhes/' + id);
  }

}
