import { Disciplina } from '../../disciplina/models/disciplina.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { DataFormatoUtils } from './../../util/dataFormatoUtils';
import { Aluno } from '../models/aluno.model';
import { AlunoService } from '../services/aluno.service';
import { DisciplinaListComponent } from '../../disciplina/disciplina-list/disciplina-list.component';

@Component({
  selector: 'app-aluno-form',
  templateUrl: './aluno-form.component.html',
  styleUrls: ['./aluno-form.component.css'],
  providers: [MessageService]
})
export class AlunoFormComponent implements OnInit {

  public aluno: Aluno = new Aluno();
  public form: FormGroup;
  public listaDisciplinas: Disciplina[];
  public listaDisciplinas2: Disciplina[];
  public listaDisciplinasAux: Array<Disciplina> = [];
  public title: string = 'Cadastro';
  public pt = DataFormatoUtils.formatarDataBrasil();
  public submitted: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private alunoService: AlunoService,
    private disciplinaList: DisciplinaListComponent,
    private location: Location
  ) { }

  ngOnInit() {
    this.construirForm();
    this.route.params.subscribe( aluno => {
      if (aluno['id']) { 
        this.title = "Atualização";
        this.disciplinaList.listarDisciplinas();
        this.listaDisciplinas = this.converteDropDown(this.disciplinaList.getListaDisciplina());
        this.aluno = this.alunoService.obterAluno(aluno['id']);
        this.form.get('nome').setValue(this.aluno.nome);
        this.form.get('cpf').setValue(this.aluno.cpf);
        this.form.get('dataNascimento').setValue(this.aluno.dataNascimento);
        this.form.get('matricula').setValue(this.aluno.matricula);
        this.obterDisciplinas();
      }
      else{
        this.title = "Cadastro";
        this.disciplinaList.listarDisciplinas();
        this.listaDisciplinas = this.converteDropDown(this.disciplinaList.getListaDisciplina());
      }
    }); 
  }

  construirForm() {
    this.form = this.formBuilder.group({
      id: [null],
      nome: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
      cpf: ['', [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
      dataNascimento: [new Date(), [Validators.required]],
      matricula: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
      disciplinas: [[]]
    })
  }

  converteDropDown(arr) {
    return arr.map(disc => {
      return {
        value: disc.id,
        label: disc.nome
      }
    })
  }

  obterDisciplinas() {
    if (this.aluno.disciplinas) {
      this.listaDisciplinas2 = this.disciplinaList.getListaDisciplina();
      this.aluno.disciplinas.map(discAluno => {
        this.listaDisciplinas2.map(disc => {
          if(discAluno.id === disc.id){
            this.form.value.disciplinas.push(disc.id);
          }
        });
      });
    }
  }

  vincularDisciplinas() {
    this.listaDisciplinas2 = this.disciplinaList.getListaDisciplina();
    this.listaDisciplinasAux = [];
    this.listaDisciplinas2.map(disc => {
      this.form.value.disciplinas.some(sel => {
        if(sel === disc.id){
          this.listaDisciplinasAux.push(disc);
        }
      });
    });
    this.aluno.disciplinas = this.listaDisciplinasAux;
  }

  onSubmit() {
    this.submitted = true;
    //todos os dados estiverem corretos
    if(this.form.valid) {
      this.aluno.nome = this.form.value.nome;
      this.aluno.cpf = this.form.value.cpf;
      this.aluno.matricula = this.form.value.matricula;
      this.aluno.dataNascimento = this.form.value.dataNascimento;
      this.form.value.disciplinas = this.vincularDisciplinas();
      this.alunoService.save(this.aluno, this.title);
      this.router.navigate(['/alunos']);
    }
    else{
      alert('Ainda existem campos incorretos!');
    }
  }

  onCancel(){
    this.submitted = false;
    this.form.reset();
    this.location.back();
  }

}
