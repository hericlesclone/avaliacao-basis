
export class AlunoListagemDTO {
  id?: number;
  nome?: string;
  matricula?: string;
  idade?: number;
}