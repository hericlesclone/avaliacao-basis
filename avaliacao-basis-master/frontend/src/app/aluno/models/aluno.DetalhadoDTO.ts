import { Disciplina } from '../../disciplina/models/disciplina.model';

export class AlunoDetalhadoDTO {
  id?: number;
  nome?: string;
  matricula?: string;
  disciplinas ?: Disciplina[];
}