import { Component, OnInit } from '@angular/core';
import { Disciplina } from '../models/disciplina.model';
import { DisciplinaListagemDTO } from '../models/disciplina.ListagemDTO';
import { DisciplinaDetalhadoDTO } from '../models/disciplina.DetalhadoDTO';
import { DisciplinaService } from '../services/disciplina.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-disciplina-list',
  templateUrl: './disciplina-list.component.html',
  styleUrls: ['./disciplina-list.component.css']
})
export class DisciplinaListComponent implements OnInit {

  disciplinas: Disciplina[];
  disciplinaDetalhada: DisciplinaDetalhadoDTO;
  DisciplinaListagemDTO: DisciplinaListagemDTO;

  constructor(
    private disciplinaService: DisciplinaService,
    private confirmationService: ConfirmationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.listarDisciplinas();
  }

  getListaDisciplina(): Disciplina[]{
    return this.disciplinas;
  }

  listarDisciplinas() {
    this.disciplinaService.listar().subscribe(dados => {
      this.disciplinas = dados;
      //alert(dados);
    });
  }

  deletar(disciplina: Disciplina) {
    this.disciplinaService.deletar(disciplina.id).subscribe(dados => {
      this.listarDisciplinas();
    }, erro => {
      alert(erro.error.text);
      this.listarDisciplinas();
    });
  }

  ListagemDTO(id: number){
    this.disciplinaService.getListagemDTO(id).subscribe(dados => {
      this.DisciplinaListagemDTO = dados;
      alert('Id: ' + this.DisciplinaListagemDTO.id + '\n'
            + 'Nome: ' + this.DisciplinaListagemDTO.nome + '\n'
            + 'Descrição: ' + this.DisciplinaListagemDTO.descricao + '\n'
            + 'Carga Horária: ' + this.DisciplinaListagemDTO.cargaHoraria + 'hrs \n');
    });
  }

  DetalhadoDTO(id: number){
    this.disciplinaService.getDetalhadoDTO(id).subscribe(dados => {
      this.disciplinaDetalhada = dados;
      alert('Id: ' + this.disciplinaDetalhada.id + '\n'
            + 'Nome: ' + this.disciplinaDetalhada.nome + '\n'
            + 'Carga Horária: ' + this.disciplinaDetalhada.cargaHoraria + 'hrs \n'
            + 'Professor: ' + this.disciplinaDetalhada.professor.nome + '\n'
            + '    - id: ' + this.disciplinaDetalhada.professor.id + '\n'
            + '    - matricula: ' + this.disciplinaDetalhada.professor.matricula + '\n'
            + '    - área de atuação: ' + this.disciplinaDetalhada.professor.area_atuacao + '\n'
            + '    - data de nascimento: ' + this.disciplinaDetalhada.professor.dataNascimento + '\n');
    });
  }
}
