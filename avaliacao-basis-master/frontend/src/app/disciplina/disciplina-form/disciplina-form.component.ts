import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataFormatoUtils } from './../../util/dataFormatoUtils';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ProfessorListComponent } from '../../professor/professor-list/professor-list.component';
import { Disciplina } from './../models/disciplina.model';
import { Professor } from '../../professor/models/professor.model';
import { DisciplinaService } from '../services/disciplina.service';
import { ProfessorService } from '../../professor/services/professor.service';

@Component({
  selector: 'app-disciplina-form',
  templateUrl: './disciplina-form.component.html',
  styleUrls: ['./disciplina-form.component.css'],
  providers: [MessageService]
})
export class DisciplinaFormComponent implements OnInit {

  public disciplina: Disciplina = new Disciplina();
  public professor: Professor = new Professor();
  public form: FormGroup;
  public listaProfessores: Professor[];
  public listaProfessores2: Professor[];
  public title: string = 'Cadastro';
  public pt = DataFormatoUtils.formatarDataBrasil();
  public submitted: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private disciplinaService: DisciplinaService,
    private location: Location,
    public professorList : ProfessorListComponent,
  ) { }

  ngOnInit() {
    this.construirForm();
    this.route.params.subscribe( disciplina => {
      if (disciplina['id']) { 
        this.title = "Atualização";
        this.professorList.listarProfessores();
        this.listaProfessores = this.converteDropDown(this.professorList.getListaProfessor());
        this.disciplina = this.disciplinaService.obterDisciplina(disciplina['id']);
        this.form.value.opcoes = this.disciplina.ativa;
        this.form.get('nome').setValue(this.disciplina.nome);
        this.form.get('descricao').setValue(this.disciplina.descricao);
        this.form.get('cargaHoraria').setValue(this.disciplina.cargaHoraria);
        this.obterProfessor();
      }
      else{
        this.title = "Cadastro";
        this.professorList.listarProfessores();
        this.listaProfessores = this.converteDropDown(this.professorList.getListaProfessor());
      }
    }); 
  }

  obterProfessor() {
    if (this.disciplina.professor) {
      this.listaProfessores2 = this.professorList.getListaProfessor();
      this.listaProfessores2.map(disc => {
        if(this.disciplina.professor.id === disc.id){
          this.form.value.professores.push(disc.id);
        }
      });
    }
  }

  construirForm() {
    this.form = this.formBuilder.group({
      id: [null],
      nome: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(50)]],
      descricao: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]],
      cargaHoraria: ['', [Validators.required, Validators.minLength(0), Validators.maxLength(5)]],
      opcoes: [[]],
      professores: [[]]
    })
  }

  converteDropDown(arr) {
    return arr.map(prof => {
      return {
        value: prof.id,
        label: prof.nome
      }
    })
  }

  onSubmit() {
    this.submitted = true;
    //todos os dados estiverem corretos
    if(this.form.valid) {
      this.disciplina.nome = this.form.value.nome;
      this.disciplina.descricao = this.form.value.descricao;
      this.disciplina.cargaHoraria = this.form.value.cargaHoraria;
      this.vincularOpcao();
      this.vincularProfessor();
      this.disciplinaService.save(this.disciplina, this.title);
      //this.router.navigate(['/disciplinas']);
    }
    else{
      alert('Ainda existem campos incorretos!');
    }
  }

  vincularProfessor() {
    this.listaProfessores2 = this.professorList.getListaProfessor();
    this.listaProfessores2.map(prof => {
      this.form.value.professores.some(sel => {
        if(sel === prof.id){
          this.disciplina.professor = prof;
        }
      });
    });
  }

  vincularOpcao(){
    if(this.form.value.opcoes){
      this.disciplina.ativa = 1;
    }
    else{
      this.disciplina.ativa = 0;
    }
  }

  onCancel(){
    this.submitted = false;
    this.form.reset();
    this.location.back();
  }

}
