import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ConfirmDialogModule } from 'primeng/confirmdialog';
import {ConfirmationService } from 'primeng/api';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';
import { PanelModule, Panel } from 'primeng/panel';
import { ToastModule } from 'primeng/toast';
import { CalendarModule } from 'primeng/calendar';
import { MultiSelectModule } from 'primeng/multiselect';
import { DisciplinaListComponent } from './disciplina-list/disciplina-list.component';
import { DisciplinaFormComponent } from './disciplina-form/disciplina-form.component';
import { ProfessorListComponent } from '../professor/professor-list/professor-list.component';
import { DisciplinaService } from './services/disciplina.service';
import { ProfessorService } from '../professor/services/professor.service'
import { HttpClientModule } from '@angular/common/http';
import { MatSliderModule } from '@angular/material/slider';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DatePipe } from '@angular/common';


@NgModule({
  declarations: [
    DisciplinaListComponent,
    DisciplinaFormComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ButtonModule,
    ConfirmDialogModule,
    TableModule,
    InputMaskModule,
    InputTextModule,
    PanelModule,
    ToastModule,
    CalendarModule,
    MultiSelectModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSliderModule,
    NgMultiSelectDropDownModule.forRoot(),
  ],
  providers: [
    ConfirmationService,
    DisciplinaService,
    ProfessorService,
    DatePipe,
    ProfessorListComponent,
  ]
})
export class DisciplinaModule { }
