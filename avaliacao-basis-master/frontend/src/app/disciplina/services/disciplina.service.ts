import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Disciplina } from '../models/disciplina.model';

@Injectable({
  providedIn: 'root'
})
export class DisciplinaService {
  private url = 'http://localhost:8080/api/disciplinas/';
  disciplinas: Disciplina[] = [];
  disciplina: Disciplina;
  
  constructor(private http: HttpClient) { }

  listar(): Observable<any>{
    //obtem lista de disciplinas
    return this.http.get(this.url);
  }

  // apaga disciplina por id
  deletar(id: number): Observable<any>{
    return this.http.delete(this.url + id);
  }

  public obterDisciplina(id: number): Disciplina {
    // obtem disciplina por id
      this.listar().subscribe(dados => {
        this.disciplinas = dados;
        this.disciplinas.map(professor => {
          if(professor.id == id){
            this.disciplina = professor;
          }
        });
      });
      return this.disciplina;
  }

  public save(disciplina: Disciplina, title : string) {
    if(title === "Cadastro"){
      this.store(disciplina).subscribe(dados => {//cadastrar
        alert(dados);
      }, erro => {
        alert(erro.error.text);
      });
    }
    else{
      this.update(disciplina).subscribe(dados => {//atualizar
        alert(dados);
      }, erro => {
        alert(erro.error.text);
      });
    }
  }

  private store(disciplina: Disciplina): Observable<any>{
    //salva disciplina
    return this.http.post(this.url, disciplina);
  }

  private update(disciplina: Disciplina): Observable<any>{
    // atualiza disciplina
    return this.http.put(this.url, disciplina);
  }

  getListagemDTO(id: number): Observable<any>{
    return this.http.get(this.url + 'listagem/' + id);
  }

  getDetalhadoDTO(id: number): Observable<any>{
    return this.http.get(this.url + 'detalhes/' + id);
  }
}
