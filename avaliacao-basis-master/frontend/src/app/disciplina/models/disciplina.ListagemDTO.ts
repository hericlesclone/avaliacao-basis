
export class DisciplinaListagemDTO {
  id?: number;
  nome?: string;
  descricao?: string;
  cargaHoraria?: number;
}