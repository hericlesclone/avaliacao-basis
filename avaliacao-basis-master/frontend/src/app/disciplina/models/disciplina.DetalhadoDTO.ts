import { Professor } from '../../professor/models/professor.model';

export class DisciplinaDetalhadoDTO {
  id?: number;
  nome?: string;
  cargaHoraria?: number;
  professor ?: Professor;
}