import { Component, OnInit } from '@angular/core';
import { Professor } from '../models/professor.model';
import { ProfessorService } from '../services/professor.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { ProfessorDetalhadoDTO } from '../models/professor.DetalhadoDTO';
import { stringify } from 'querystring';

@Component({
  selector: 'app-professor-list',
  templateUrl: './professor-list.component.html',
  styleUrls: ['./professor-list.component.css']
})
export class ProfessorListComponent implements OnInit {

  professores: Professor[];
  professorDetalhado: ProfessorDetalhadoDTO;
  texto: string;

  constructor(
    private professorService: ProfessorService,
    private confirmationService: ConfirmationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.listarProfessores();
  }

  listarProfessores() {
    this.professorService.listar().subscribe(dados => {
      this.professores = dados;
    });
  }

  getListaProfessor(): Professor[]{
    return this.professores;
  }

  deletar(professor: Professor) {
    this.professorService.deletar(professor.matricula).subscribe(dados => {
      this.listarProfessores();
    }, erro => {
      alert(erro.error.text);
      this.listarProfessores();
    });
  }

  DetalhadoDTO(id: number){
    this.professorService.getDetalhadoDTO(id).subscribe(dados => {
      this.professorDetalhado = dados;
      this.texto = 'Id: ' + this.professorDetalhado.id + '\n'
                + 'Nome: ' + this.professorDetalhado.nome + '\n'
                + 'Matrícula: ' + this.professorDetalhado.matricula + '\n';
      this.professorDetalhado.disciplinas.map(disciplina =>{
        this.texto = this.texto + '   - Disciplina: ' + disciplina.nome + '\n'
                                + '      - id: ' + disciplina.id + '\n'
                                + '      - descrição: ' + disciplina.descricao + '\n'
                                + '      - ativa: ' + disciplina.ativa + '\n'
                                + '      - carga horária: ' + disciplina.cargaHoraria + 'hrs\n';
      });
      alert(this.texto);
    });
  }
}
