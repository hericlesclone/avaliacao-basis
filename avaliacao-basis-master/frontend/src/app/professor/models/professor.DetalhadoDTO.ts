import { Disciplina } from '../../disciplina/models/disciplina.model';

export class ProfessorDetalhadoDTO {
    id?: number;
    nome?: string;
    matricula?: string;
    disciplinas ?: Disciplina[];
}