export class Professor {
    id?: number;
    matricula?: string;
    nome?: string;
    area_atuacao?: string;
    dataNascimento?: Date;
}