import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataFormatoUtils } from './../../util/dataFormatoUtils';
import { Professor } from '../models/professor.model';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MessageService } from 'primeng/api';
import { ProfessorService } from '../services/professor.service';

@Component({
  selector: 'app-professor-form',
  templateUrl: './professor-form.component.html',
  styleUrls: ['./professor-form.component.css'],
  providers: [MessageService]
})
export class ProfessorFormComponent implements OnInit {

  public professor: Professor = new Professor();
  public form: FormGroup;
  public title: string = 'Cadastro';
  public pt = DataFormatoUtils.formatarDataBrasil();
  public submitted: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private professorService: ProfessorService,
    private location: Location
  ) { }

  ngOnInit() {
    this.construirForm();
    this.route.params.subscribe( professor => {
      if (professor['id']) { 
        this.title = "Atualização";
        this.professor = this.professorService.obterProfessor(professor['id']);
        this.form.get('nome').setValue(this.professor.nome);
        this.form.get('area_atuacao').setValue(this.professor.area_atuacao);
        this.form.get('dataNascimento').setValue(this.professor.dataNascimento);
        this.form.get('matricula').setValue(this.professor.matricula);
      }
      else{
        this.title = "Cadastro";
      }
    });
  }

  construirForm() {
    this.form = this.formBuilder.group({
      id: [null],
      nome: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
      matricula: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
      area_atuacao: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]],
      dataNascimento: [new Date(), [Validators.required]]
    })
  }

  onSubmit() {
    this.submitted = true;
    if(this.form.valid){
      this.professor.nome = this.form.value.nome;
      this.professor.matricula = this.form.value.matricula;
      this.professor.area_atuacao = this.form.value.area_atuacao;
      this.professor.dataNascimento = this.form.value.dataNascimento;
      this.professorService.save(this.professor, this.title);
      this.router.navigate(['/professores']);
    }
    else{
      alert('Ainda existem campos incorretos!');
    }
  }

  onCancel(){
    this.submitted = false;
    this.form.reset();
    this.location.back();
    this.router.navigate(['/professores']);
  }

}
