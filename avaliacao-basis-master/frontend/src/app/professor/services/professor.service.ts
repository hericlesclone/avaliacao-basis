import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Professor } from '../models/professor.model';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ProfessorService {
  private url = 'http://localhost:8080/api/professores/';
  professores: Professor[] = [];
  professor: Professor;

  constructor(
    private http: HttpClient,  
    private datePipe: DatePipe) { }

  //obtem lista de professores
  listar(): Observable<any>{
    return this.http.get(this.url);
  }

  // apaga professor por matricula
  deletar(matricula): Observable<any>{
    return this.http.delete(this.url + matricula);
  }

  save(professor: Professor, title : string) {
    if(title === "Cadastro"){
      this.store(professor).subscribe(dados => {//cadastrar
        alert(dados);
      }, erro => {
        alert(erro.error.text);
      });
    }
    else{
      this.update(professor).subscribe(dados => {//atualizar
        alert(dados);
      }, erro => {
        alert(erro.error.text);
      });
    }
  }

  obterProfessor(id : number): Professor{
    this.listar().subscribe(dados => {
      this.professores = dados;
      this.professores.map(professor => {
        if(professor.id == id){
          this.professor = professor;
          this.professor.dataNascimento = new Date(this.datePipe.transform(professor.dataNascimento, 'dd-MM-yyyy'));
        }
      });
    });
    return this.professor;
  }

  //salva um novo aluno
  store(professor: Professor): Observable<any>{
    professor.dataNascimento = new Date(this.datePipe.transform(professor.dataNascimento, 'yyyy-MM-dd'));
    return this.http.post(this.url, professor);
  }

  // atualiza aluno
  update(professor: Professor): Observable<any>{
    professor.dataNascimento = new Date(this.datePipe.transform(professor.dataNascimento, 'yyyy-MM-dd'));
    return this.http.put(this.url, professor);
  }

  getDetalhadoDTO(id: number): Observable<any>{
    return this.http.get(this.url + 'detalhes/' + id);
  }
}
